#include <Servo.h>
Servo srv;
#define pinServo 8

#include "ControlMotor.h"
#include "ControlMotor.c"

#include "Senzori.h"
#include "Senzori.c"

float distance_front;
float distance_left;
float distance_right;
float distance_back;

float light_right;
float light_left;

int state;
int carriage_return;
int new_line;

void buzzerTone()
{
  tone(buzzer,1000,500);
}

void initServo(int pin)
{ 
  srv.attach(pin);
  srv.write(30);
  delay(1000);
  srv.detach();
}

void setup() {

  initServo(pinServo);
  analogReference(DEFAULT);
  
  Serial.begin(9600);
  Serial1.begin(9600);
  
  digitalWrite(motor1_f, 0);
  digitalWrite(motor1_b, 0);
  digitalWrite(motor2_f, 0);
  digitalWrite(motor2_b, 0);


  pinMode (motor1_f, OUTPUT);
  pinMode (motor1_b, OUTPUT);
  pinMode (motor2_f, OUTPUT);
  pinMode (motor2_b, OUTPUT);

  pinMode(trigPin,OUTPUT);
  pinMode(echoPin,INPUT);
  pinMode(trigPin2,OUTPUT);
  pinMode(echoPin2,INPUT);

  pinMode(buzzer,OUTPUT);

  pinMode(pinLumina1,INPUT);
  pinMode(pinLumina2,INPUT);

}
//functie prin care se citeste un caracter de la serial, reprezentand starea pe care urmeaza
//sa o ia masina
void readFromSerial()
{
if(Serial1.available())
    {
      delay(3);
      state=Serial1.read();
      state=state-'0';
      carriage_return=Serial1.read();
      new_line=Serial1.read();
     
    } 
}

//dupa ce s-a citit de pe serial caracterul,reprezentand starea masinii, masina se va misca intr-o anumita directie
// in functie de starea prezenta,cu exceptia cazului in care ne aflam in starea 6, care va incerca sa urmareasca lumina
void controlCar()
{
  
switch(state)
{
  case 1:
    if(state!=1)
    changeDirectionDelay(20);
    move_forward(128);
    break;

  case 2:
    if(state!=2)
    changeDirectionDelay(20);
    move_back(128);
    break;

  case 3:
    if(state!=3)
    changeDirectionDelay(20);
    move_left(128);
    break;

  case 4:
    if(state!=4)
    changeDirectionDelay(20);
    move_right(128);
    break;

   case 6:
    followLight();
 
    break;

  case 0:
    move_stop();

  default:
    move_stop();
    break;
}
}

// functie pentru a prevenii accidentariile masinii
void no_suicide()
{

  if(distance_back<1 || distance_front<1) //pentru intializare, sa nu porneasca masina in prima secunda crezand ca exista un obstacol foarte aproape                                         
    return;                               //(pana se pornesc senzorii, acestia vor avea valoarea 0);

  if(distance_front<=10 && distance_back<=10 && (state==1 || state==2 ))  //in cazul in care exista obstacole atat in fata cat si in spate, masina se opreste si suntem alertati
  {                                            //de buzzer ca masina este blocata si trebuie sa folosim directiile stanga sau dreapta
    move_stop();
    buzzerTone();
    return;
  }
  
  if(distance_front<15 && distance_front>10 && state!=2)    // in cazul in care avem un obstacol in fata, si nu incercam sa dam cu spatele(state 2)
  {
    
    if(state!=10)                                         //prima data cand se observa obstacolul, se trece in starea 10 pentru a oprii masina si primim
      buzzerTone();                                       //un buzzer de avertizare

    if(state!=6)                                          //cu exceptia cazului in care masina se afla in starea de urmarire a luminii
    state=10;

    changeDirectionDelay(20);
    move_stop();
  }
 else if(distance_front<=10)                    //daca obiectul incepe sa se apropie din fata, si exista loc in spate, masina va incepe sa mearga in spate
 {                                              //pana cand distanta este cel putin 15(cazul anterior), dupa care se opreste

   if(distance_back>25)
   {
    changeDirectionDelay(20);
    move_back(128);
    buzzerTone();
   delay(300);
  }
   
  }
  
   // la fel ca si cazul prezentat anterior, cu senzorul din fata, se va verifica si pentru senzorul din spate aceleasi cazuri.
  if(distance_back<15 && distance_back>10 && state!=1)
  {

    if(state!=10)
      buzzerTone();
    
    if(state!=6)  
    state=10;


    changeDirectionDelay(20);
    move_stop();
    
  }
  
  else if(distance_back<=10)
 {
    if(distance_front>25)
   {
    changeDirectionDelay(20);
    move_forward(128);
    buzzerTone();
    delay(300);
   }
    
 }
 
}

//functie pentru starea de urmarire lumina
//masina va incerca tot timpul sa fie cu fata inspre lumina, pentru a o putea urmarii
// se vor folosii 2 senzori de lumina, unul in partea stanga, celalalt in partea dreapta
//iar in functie de valoriile acestora, masina se va intoarce inspre senzorul cel mai departe de lumina,
//pentru a putea ajunge cu fata spre aceasta
void followLight()
{
  
  

  if( (((light_right + light_left)/2) >30) )  // ca sa nu se confunde cu lumina din camera, se verifica ca avem o sursa de lumina mai puternica in apropiere
  {
    Serial1.print("right");
    
    Serial1.println(light_right);
    Serial1.print("left");
    Serial1.println(light_left);

    Serial1.println();

    if(light_right>light_left && ((light_right-light_left-35)>0 ))  // daca lumina din dreapta e mai aproape, ne rotim stanga
    {
      changeDirectionDelay(20);
      move_left(128);
      delay(100);
      return;
    }
    
    if(light_left>light_right && ((light_left-light_right-35)>0 ))  // daca lumina din stanga e mai aproape, ne rotim dreapta
    {
      changeDirectionDelay(20);
      move_right(128);
      delay(100);
      return;
      
    }
                                                      
      
      changeDirectionDelay(20);                         // altfel, mergem in fata, stiind ca lumina se afla in fata robotului, daca avem loc

      if(distance_front>30)
      {move_forward(128);                               
      delay(100);
      }
      else{
        buzzerTone();
      }
  }
  else{
  move_stop();
  
  }

}


void loop() {
  

  readFromSerial();
  delay(10);

  
  distance_front=generateDistance(1,echoPin,trigPin);
  distance_back=generateDistance(1,echoPin2,trigPin2);

  light_right=getLight(pinLumina1);
  light_left=getLight(pinLumina2);

    delay(10);

  controlCar();
  
    delay(10);

  no_suicide();

   if(state==6)
     followLight();
     

}
