#define motor1_f 4
#define motor1_b 5
#define motor2_f 7
#define motor2_b 6

void move_forward(int speed);

void move_back(int speed);

void move_left(int speed);

void move_right(int speed);

void move_stop();

void changeDirectionDelay(int ms);