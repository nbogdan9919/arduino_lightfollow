#include <Arduino.h>
#include "Senzori.h"

float generateDistance(int cnt,int echoPinn,int trigPinn)
{
 float total=0;
 float sound_speed =0.0343;
  
  for(int i=0;i<cnt;i++)
  {
    float dist=0;
    int duration=0;
    
  digitalWrite(trigPinn,0); // ne asiguram ca pinul este setat pe 0
  delay(2);

  
  digitalWrite(trigPinn,1);    //conform manualului de utilizare,
  delay(10);        //avem nevoie de un puls de 10 ms
  digitalWrite(trigPinn,0);    //pt a trimite 8 cicluri de semnale 
                //ultrasonice
  
 
  duration=pulseIn(echoPinn,HIGH,1000000); // duration va masura timpul
                  //necesar semnalului de a se
                  //intoarce
    
  dist=((duration*sound_speed)/2); // timpul pentru a ajunge la obiect
                  // este jumatate din timpul necesar
                  //pt a ajunge si inapoi
                  //iar viteza cu care se misca semnalul
                  // este viteza sunetului.
                    
                    
                    
  total=total+dist;
  }
  
  return total/cnt;

}

int getLight(int pin)
{
  int valoareLumina=analogRead(pin);

  return valoareLumina;

}
