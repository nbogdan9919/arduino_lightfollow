#include <Arduino.h>
#include "ControlMotor.h"

void move_forward(int speed)
{
  analogWrite(motor1_f,speed);
  digitalWrite(motor1_b,0);

  analogWrite(motor2_f,speed);
  digitalWrite(motor2_b,0);

}

void move_back(int speed)
{
	
  digitalWrite(motor1_f,0);
  analogWrite(motor1_b,speed);

  digitalWrite(motor2_f,0);
  analogWrite(motor2_b,speed);

}

void move_left(int speed)
{
   digitalWrite(motor1_f,0);
   analogWrite(motor1_b,speed);

   digitalWrite(motor2_f,0);
   digitalWrite(motor2_b,0); 

}

void move_right(int speed)
{
    analogWrite(motor1_f,speed);
    digitalWrite(motor1_b,0);

    digitalWrite(motor2_f,0);
    digitalWrite(motor2_b,0); 
}

void move_stop()
{
   digitalWrite(motor1_f,0);
   digitalWrite(motor1_b,0);

   digitalWrite(motor2_f,0);
   digitalWrite(motor2_b,0); 
}


void changeDirectionDelay(int ms)
{
  move_stop();
  delay(ms);
  
  
}

